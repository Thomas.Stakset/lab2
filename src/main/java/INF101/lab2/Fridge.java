package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    public int size;
    public ArrayList<FridgeItem> items;

    public Fridge(){
        this.items = new ArrayList<FridgeItem>();
        this.size = 20;
    }
    
    @Override
    public int totalSize() {
        return this.size;
    }

    @Override
    public int nItemsInFridge() {
        return this.items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        this.items.add(item);
        if (nItemsInFridge() <= 20)
            return true;
        else;
            return false;
    }

    @Override
    public void takeOut(FridgeItem item) {

        if(nItemsInFridge() <= 0)
        throw new NoSuchElementException();
        else this.items.remove(item);
        
    }

    @Override
    public void emptyFridge() {
        this.items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for(int i=0; i < nItemsInFridge(); i++) {
            FridgeItem item = items.get(i);
            if(item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        for(FridgeItem expiredItem : expiredFood) {
            items.remove(expiredItem);
        }
        return expiredFood;
    }
    
}
